//
//  StorageService.swift
//  Englishgram
//
//  Created by Rinat on 09.09.2021.
//

import Swinject
import RxSwift
import FirebaseStorage

struct StorageData {
    let url: URL?
}

enum StorageServiceChildType: String {
    case profileImages = "profile_images"
    case posts = "posts_images"
}

protocol StorageServiceProtocol: AnyObject {
    func putData(child: StorageServiceChildType, fileName: String, uploadData: Data) -> Single<StorageData>
}

final class StorageService: StorageServiceProtocol {
    func putData(child: StorageServiceChildType, fileName: String, uploadData: Data) -> Single<StorageData> {
        return Single<StorageData>.create { single in
            let ref = Storage.storage().reference().child(child.rawValue).child(fileName)
            
                ref.putData(uploadData, metadata: nil) { storageMetadata, error in
                    
                    if let error = error {
                        single(.error(error))
                        return
                    }
                    
                    ref.downloadURL { url, error in
                        if let err = error {
                            single(.error(err))
                            return
                        }
                        single(.success(StorageData(url: url)))
                    }
            }
            
            return Disposables.create()
        }

    }
}

class StorageServiceAssembly: Assembly {
    func assemble(container: Container) {
        container.register(StorageServiceProtocol.self, factory: { r in
            return StorageService()
        }).inObjectScope(.transient)
    }
}
