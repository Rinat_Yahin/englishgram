//
//  UserServiceProtocol.swift
//  ECExtra
//
//  Created by Rinat on 21.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Foundation
import RxSwift
import Swinject

protocol UserServiceProtocol: AnyObject {
    func createUser(email: String, password: String, name: String) -> Single<User>
    func login(email: String, password: String, completion: @escaping (Result<User, Error>) -> Void)
}

final class UserService: UserServiceProtocol {
    
    func createUser(email: String, password: String, name: String) -> Single<User> {
        return Single<User>.create { single in
            FirebaseCreateUserRequest(email: email, password: password, name: name).request { result in
                switch result {
                case .success(let user):
                    return single(.success(user))
                    
                case .failure(let error):
                    return single(.error(error))
                }
            }
            return Disposables.create()
        }
    }
    
    func login(email: String, password: String, completion: @escaping (Result<User, Error>) -> Void) {
        FirebaseLoginUserRequest(email: email, password: password).request(completion: completion)
    }
}

class UserServiceAssembly: Assembly {
    func assemble(container: Container) {
        container.register(UserServiceProtocol.self, factory: { r in
            return UserService()
        }).inObjectScope(.transient)
    }
}
