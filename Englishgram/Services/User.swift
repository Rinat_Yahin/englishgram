//
//  User.swift
//  ECExtra
//
//  Created by Rinat on 21.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Foundation

struct User {
    var name: String?
    var email: String?
    var uid: String?
    var profileImageUrl: String?
    
    init(name: String?, email: String?, uid: String?) {
        self.name = name
        self.email = email
        self.uid = uid
    }
    
    init(uid: String, dictionary: [String: Any]) {
        self.uid = uid
        self.name = dictionary["username"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"]  as? String ?? ""
    }
}
