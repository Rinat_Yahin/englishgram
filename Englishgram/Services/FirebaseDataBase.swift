//
//  FirebaseDataBase.swift
//  Englishgram
//
//  Created by Rinat on 09.09.2021.
//

import Swinject
import RxSwift
import FirebaseDatabase

enum FirebaseDataBaseError: Error {
    case emptyMetadata
}

enum FirebaseDataBaseChildType: String {
    case users = "users"
    case posts = "posts"
    case following = "following"
}

protocol FirebaseDataBaseProtocol: AnyObject {
    func updateChildValues(ref: DatabaseReference, values: [AnyHashable: Any]) -> Single<DatabaseReference>
    func observeSingleEvent(ref: DatabaseReference) -> Single<DataSnapshot>
}

final class FirebaseDataBase: FirebaseDataBaseProtocol {
    func updateChildValues(ref: DatabaseReference, values: [AnyHashable: Any]) -> Single<DatabaseReference> {
        Single<DatabaseReference>.create { single in
            ref.updateChildValues(values) { error, ref in
                if let error = error {
                    single(.error(error))
                    return
                }
                single(.success(ref))
            }
            return Disposables.create()
        }
    }

    func observeSingleEvent(ref: DatabaseReference) -> Single<DataSnapshot> {
        Single<DataSnapshot>.create { single in
            ref.observeSingleEvent(of: .value) { snapshot in
                print(snapshot)
                single(.success(snapshot))
            }
            return Disposables.create()
        }
    }
}

class FirebaseDataBaseAssembly: Assembly {
    func assemble(container: Container) {
        container.register(FirebaseDataBaseProtocol.self, factory: { r in
            return FirebaseDataBase()
        }).inObjectScope(.transient)
    }
}
