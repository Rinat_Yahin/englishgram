//
//  FirebaseUserRequest.swift
//  ECExtra
//
//  Created by Rinat on 21.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Firebase

final class FirebaseCreateUserRequest {
        
    private let email: String
    private let password: String
    private let name: String
    
    init(email: String, password: String, name: String) {
        self.email = email
        self.password = password
        self.name = name
    }
    
    func request(completion: @escaping ((Result<User, Error>) -> Void)) {
        let name = self.name
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let error = error {
                completion(.failure(error))
            }
            let changeRequest = authResult?.user.createProfileChangeRequest()
            changeRequest?.displayName = name
            changeRequest?.commitChanges(completion: { (error) in
                if let error = error {
                    completion(.failure(error))
                }
                let user = User(name: authResult?.user.displayName, email: authResult?.user.email, uid: authResult?.user.uid)
                completion(.success(user))
            })
        }
    }
}

final class FirebaseLoginUserRequest {
        
    private let email: String
    private let password: String
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    func request(completion: @escaping ((Result<User, Error>) -> Void)) {
        Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            if let error = error {
                completion(.failure(error))
            }
            let user = User(name: authResult?.user.displayName, email: authResult?.user.email, uid: authResult?.user.uid)
            completion(.success(user))
        }
    }
}
