//
//  Post.swift
//  Englishgram
//
//  Created by Rinat on 10.09.2021.
//

import Foundation

struct Post {
    var id: String?
    
    let imageUrl: String
    let user: User
    let caption: String
    
    var hasLiked = false
    init(user: User, dictionary: [String: Any]) {
        self.user = user
        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
        self.caption = dictionary["caption"] as? String ?? ""
    }
}
