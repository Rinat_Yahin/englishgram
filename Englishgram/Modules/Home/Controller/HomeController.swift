//
//  HomeHomeController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewController: BaseViewController {

    // MARK: Properties
    private let bag = DisposeBag()
    private let viewModel: HomeViewModel

    private lazy var rootView: HomeView = {
        let view = HomeView()
        view.viewController = self
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel

        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        setupPhotoSelector()
        viewModel.fetchFollowingUserIds()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: updateFeedNotificationName, object: nil)
    }
    
    private func bindViewModel() {
        viewModel.postsSubject.subscribe(onNext: {
            self.rootView.posts = $0
        }).disposed(by: bag)
    }
    
    override func loadView() {
        self.view = rootView
    }

    @objc func handleUpdateFeed() {
        viewModel.posts = []
        viewModel.fetchFollowingUserIds()
    }

    
    fileprivate func setupPhotoSelector() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus_unselected").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handlePhotoSelector))
    }
    
    @objc func handlePhotoSelector() {
        viewModel.coordinator?.showPhotoSelector()
    }

}
