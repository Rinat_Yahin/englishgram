//
//  HomeHomeDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class HomeDIAssembly: Assembly {

    func assemble(container: Container) {
        container.register(HomeViewController.self) { (r: Resolver, coordinator: RootCoordinator) in
            let viewModel = HomeViewModel(dataBaseService: r.resolve(FirebaseDataBaseProtocol.self)!)
            viewModel.coordinator = coordinator
            return HomeViewController(viewModel: viewModel)
        }
    }
}
