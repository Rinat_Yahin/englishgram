//
//  HomeHomeViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import Foundation
import Firebase
import RxSwift

class HomeViewModel {
    let postsSubject = PublishSubject<[Post]>()
    let errorSubject = PublishSubject<String>()
    weak var coordinator: RootCoordinator?
    
    private let bag = DisposeBag()
    private let dataBaseService: FirebaseDataBaseProtocol

    init(dataBaseService: FirebaseDataBaseProtocol) {
        self.dataBaseService = dataBaseService
    }
    
    func fetchFollowingUserIds() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.following.rawValue).child(uid)
        
        dataBaseService.observeSingleEvent(ref: ref).subscribe(onSuccess: { snapshot in
            guard let userIdsDictionary = snapshot.value as? [String: Any] else { return }
            
            userIdsDictionary.forEach({ (key, value) in
                self.fetchUser(uid: key)
                
            })
        }).disposed(by: bag)
    }

    func fetchUser(uid: String) {
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.users.rawValue).child(uid)
        
        dataBaseService.observeSingleEvent(ref: ref).map { snapshot -> User? in
            guard let userDictionary = snapshot.value as? [String: Any] else { return nil }
            let user = User(uid: uid, dictionary: userDictionary)
            return user
        }.subscribe {
            switch $0 {
            case .success(let user):
                self.fetchPostWitUser(user)
            case .error(let error):
                self.errorSubject.onNext(error.localizedDescription)
            }
        }.disposed(by: bag)
    }
    
    var posts = [Post]()
    func fetchPostWitUser(_ user: User?) {
        guard let user = user else { return }
        guard let uid = user.uid else { return }
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.posts.rawValue).child(uid)
        

        dataBaseService.observeSingleEvent(ref: ref)
            .subscribe {
                switch $0 {
                case .success(let snapshot):
                    guard let dictionaries = snapshot.value as? [String: Any] else { return }

                    dictionaries.forEach({ (key, value) in
                        guard let dictionary = value as? [String: Any] else { return }

                        var post = Post(user: user, dictionary: dictionary)
                        post.id = key

                        guard let uid = Auth.auth().currentUser?.uid else { return }
                        Database.database().reference().child("likes").child(key).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                            print(snapshot)
                            
                            if let value = snapshot.value as? Int, value == 1 {
                                post.hasLiked = true
                            } else {
                                post.hasLiked = false
                            }
                            self.posts.append(post)
                            self.postsSubject.onNext(self.posts)
                            
                        }, withCancel: { (err) in
                        })
                    })

                case .error(let error):
                    self.errorSubject.onNext(error.localizedDescription)
                }
            }.disposed(by: bag)
    }
}
