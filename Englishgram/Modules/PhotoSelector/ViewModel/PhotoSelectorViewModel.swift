//
//  PhotoSelectorPhotoSelectorViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift
import Photos

class PhotoSelectorViewModel {
    let photos = PublishSubject<[UIImage]>()
    let assets = PublishSubject<[PHAsset]>()
    let selectedImage = PublishSubject<UIImage>()
    weak var coordinator: RootCoordinator?
    
    fileprivate func assetsFetchOptions() -> PHFetchOptions {
        let fetchOptions = PHFetchOptions()
        fetchOptions.fetchLimit = 15
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchOptions.sortDescriptors = [sortDescriptor]
        return fetchOptions
    }

    func fetchPhotos() {
        let allPhotos = PHAsset.fetchAssets(with: .image, options: assetsFetchOptions())
        var images = [UIImage]()
        var assets = [PHAsset]()
        var selectedImage: UIImage?
        DispatchQueue.global(qos: .background).async {
            allPhotos.enumerateObjects({ (asset, count, stop) in
                let imageManager = PHImageManager.default()
                let targetSize = CGSize(width: 200, height: 200)
                let options = PHImageRequestOptions()
                options.isSynchronous = true
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
                    
                    if let image = image {
                        images.append(image)
                        assets.append(asset)
                        
                        if selectedImage == nil {
                            selectedImage = image
                            self.selectedImage.onNext(selectedImage!)
                        }
                    }
                    if count == allPhotos.count - 1 {
                        DispatchQueue.main.async {
                            self.photos.onNext(images)
                            self.assets.onNext(assets)
                            
                        }
                    }
                })
            })
        }
    }
    
    func showShare(image: UIImage) {
        coordinator?.showSharePhoto(image: image)
    }
}
