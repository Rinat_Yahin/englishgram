//
//  PhotoSelectorPhotoSelectorController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift

class PhotoSelectorViewController: BaseViewController {

    // MARK: Properties
    private let viewModel: PhotoSelectorViewModel
    private let bag = DisposeBag()
    private lazy var rootView: PhotoSelectorView = {
        let view = PhotoSelectorView()
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: PhotoSelectorViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    override func loadView() {
        self.view = rootView
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationButtons()
        bindViewModel()
        viewModel.fetchPhotos()
    }
    
    fileprivate func setupNavigationButtons() {
        navigationController?.navigationBar.tintColor = .black
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(handleNext))
    }
    
    func bindViewModel() {
        viewModel.photos.subscribe(onNext: { photos in
            self.rootView.images = photos
        }).disposed(by: bag)
        
        viewModel.assets.subscribe(onNext: { assets in
            self.rootView.assets = assets
        }).disposed(by: bag)
        
        viewModel.selectedImage.subscribe(onNext: { selectedImage in
            self.rootView.selectedImage = selectedImage
        }).disposed(by: bag)
    }
    
    func bindView() {
    }
    
    @objc func handleNext() {
        guard let image = rootView.selectedImage else { return }
        self.viewModel.showShare(image: image)
    }
    
    @objc func handleCancel() {
        dismiss(animated: true, completion: nil)
    }


    override var prefersStatusBarHidden: Bool {
        return true
    }

}
