//
//  PhotoSelectorPhotoSelectorDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class PhotoSelectorDIAssembly: Assembly {

    func assemble(container: Container) {
        container.register(PhotoSelectorViewController.self) { (r: Resolver, coordinator: RootCoordinator) in
            let viewModel = PhotoSelectorViewModel()
            viewModel.coordinator = coordinator
            return PhotoSelectorViewController(viewModel: viewModel)
        }
    }
}
