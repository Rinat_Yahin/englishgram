//
//  MainTabbarController.swift
//  Englishgram
//
//  Created by Rinat on 09.09.2021.
//

import UIKit

class MainTabBarController: UITabBarController {
    weak var coordinator: RootCoordinator?
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = .black
        view.backgroundColor = .white
        
        guard let items = tabBar.items else { return }
        
        for item in items {
            item.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        }

    }

}
