//
//  SharePhotoSharePhotoDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class SharePhotoDIAssembly: Assembly {

    func assemble(container: Container) {
        container.register(SharePhotoViewController.self) { (r: Resolver, coordinator: RootCoordinator, image: UIImage) in
            let storageService = r.resolve(StorageServiceProtocol.self)!
            let dataBaseService = r.resolve(FirebaseDataBaseProtocol.self)!
            let viewModel = SharePhotoViewModel(image: image, storageService: storageService,
                                                dataBaseService: dataBaseService)
            viewModel.coordinator = coordinator
                return SharePhotoViewController(viewModel: viewModel)
        }
    }
    
}
