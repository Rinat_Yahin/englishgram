//
//  SharePhotoSharePhotoViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift
import Firebase
import FirebaseDatabase
let updateFeedNotificationName = NSNotification.Name(rawValue: "UpdateFeed")

class SharePhotoViewModel {
    let image: UIImage
    let errorSubject = PublishSubject<String>()
    private let bag = DisposeBag()
    private let storageService: StorageServiceProtocol
    private let dataBaseService: FirebaseDataBaseProtocol
    weak var coordinator: RootCoordinator?
    init(image: UIImage,
         storageService: StorageServiceProtocol,
         dataBaseService: FirebaseDataBaseProtocol) {
        self.image = image
        self.storageService = storageService
        self.dataBaseService = dataBaseService
    }
    
    func sharePost(_ text: String) {
        let filename = NSUUID().uuidString
        guard let uploadData = image.jpegData(compressionQuality: 0.5) else { return }
        storageService.putData(child: .posts, fileName: filename, uploadData: uploadData).subscribe {
            switch $0 {
            case .success(let data):
                print(data)
                self.saveToDataBase(data.url?.absoluteString, caption: text)
            case .error(let error):
                self.errorSubject.onNext(error.localizedDescription)
            }
        }.disposed(by: bag)
    }
    
    func saveToDataBase(_ imageUrl: String?, caption: String) {
        guard let uid = Auth.auth().currentUser?.uid,
              let url = imageUrl else { return }
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.posts.rawValue).child(uid)
        let postRef = ref.childByAutoId()
        let values = ["imageUrl": url, "caption": caption, "imageWidth": image.size.width, "imageHeight": image.size.height, "creationDate": Date().timeIntervalSince1970] as [String : Any]
     
        
        dataBaseService.updateChildValues(ref: postRef, values: values).subscribe {
            switch $0 {
            case .success:
                self.coordinator?.dismissShareView()
                NotificationCenter.default.post(name: updateFeedNotificationName, object: nil)
            case .error(let error):
                self.errorSubject.onNext(error.localizedDescription)
            }
        }.disposed(by: bag)
    }
}
