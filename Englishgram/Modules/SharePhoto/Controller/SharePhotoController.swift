//
//  SharePhotoSharePhotoController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift
class SharePhotoViewController: BaseViewController {

    // MARK: Properties
    private let viewModel: SharePhotoViewModel
    private let bag = DisposeBag()
    private lazy var rootView: SharePhotoView = {
        let view = SharePhotoView()
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: SharePhotoViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(handleShare))
        rootView.selectedImage = viewModel.image
    }

    override func loadView() {
        self.view = rootView
        
    }
    
    @objc func handleShare() {
        setLoading(true)
        viewModel.sharePost(rootView.textView.text)
    }
    
    func bindViewModel() {
        viewModel.errorSubject.subscribe(onNext: {
            self.presentMessage(message: $0)
        }).disposed(by: bag)
    }

}
