//
//  UserProfileUserProfileViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 09/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import Foundation
import Firebase
import RxSwift
class UserProfileViewModel {
    let postsSubject = PublishSubject<[Post]>()
    
    private let dataBaseService: FirebaseDataBaseProtocol
    private let bag = DisposeBag()
    private let errorSubject = PublishSubject<String>()
    weak var coordinator: RootCoordinator?
    var user: User?
    init(dataBaseService: FirebaseDataBaseProtocol) {
        self.dataBaseService = dataBaseService
    }

    func fetchUser() -> Single<User> {
        let uid = user?.uid ?? Auth.auth().currentUser?.uid ?? ""
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.users.rawValue).child(uid)
        return dataBaseService
            .observeSingleEvent(ref: ref).map { snapshot in
                self.user = User(uid: uid, dictionary: snapshot.value as? [String: Any] ?? [:])
                return self.user!
        }
    }
    
    func fetchPost() {
        let uid = user?.uid ?? Auth.auth().currentUser?.uid ?? ""
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.posts.rawValue).child(uid)
        var posts = [Post]()
        dataBaseService.observeSingleEvent(ref: ref)
            .subscribe {
                switch $0 {
                case .success(let snapshot):
                    guard let dictionaries = snapshot.value as? [String: Any] else { return }
                    
                    dictionaries.forEach({ (key, value) in
                        guard let dictionary = value as? [String: Any] else { return }
                        guard let user = self.user else { return }
                        
                        let post = Post(user: user, dictionary: dictionary)
                        posts.append(post)
                    })
                    self.postsSubject.onNext(posts)
                    
                case .error(let error):
                    self.errorSubject.onNext(error.localizedDescription)
                }
            }.disposed(by: bag)
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
            coordinator?.start()
        } catch let signOutErr {
            print("Failed to sign out:", signOutErr)
        }
    }
    
}
