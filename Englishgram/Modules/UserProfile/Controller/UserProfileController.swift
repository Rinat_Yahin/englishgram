//
//  UserProfileUserProfileController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 09/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import RxSwift
import UIKit
import Firebase
class UserProfileViewController: BaseViewController {

    // MARK: Properties
    private let viewModel: UserProfileViewModel
    private let bag = DisposeBag()
    private lazy var rootView: UserProfileView = {
        let view = UserProfileView()
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: UserProfileViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLogOutButton()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchUser().subscribe {
            switch $0 {
            case .success(let user):
                self.rootView.user = user
                self.viewModel.fetchPost()
            
            case .error(let error):
                self.present(error: error)
            }

        }.disposed(by: bag)
    }

    private func bindViewModel() {
        viewModel.postsSubject.subscribe(onNext: {
            self.rootView.posts = $0
        }).disposed(by: bag)
    }
    
    fileprivate func setupLogOutButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogOut))
    }
    
    @objc func handleLogOut() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (_) in
            self.viewModel.signOut()
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func loadView() {
        self.view = rootView
    }

}
