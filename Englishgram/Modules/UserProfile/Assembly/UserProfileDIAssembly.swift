//
//  UserProfileUserProfileDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 09/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class UserProfileDIAssembly: Assembly {

    func assemble(container: Container) {
        container.register(UserProfileViewController.self) { (r: Resolver, coordinator: RootCoordinator, user: User?) in
            let viewModel = UserProfileViewModel(dataBaseService: r.resolve(FirebaseDataBaseProtocol.self)!)
            viewModel.coordinator = coordinator
            viewModel.user = user
            return UserProfileViewController(viewModel: viewModel)
        }
    }    
}
