//
//  UserProfilePhotoCell.swift
//  Englishgram
//
//  Created by Rinat on 10.09.2021.
//

import UIKit
import RxNuke
import RxSwift
import Nuke
class UserProfilePhotoCell: UICollectionViewCell {
    private let bag = DisposeBag()
    
    var post: Post? {
        didSet {
            guard let url = URL(string: post?.imageUrl ?? "") else { return }
            
            ImagePipeline.shared.rx.loadImage(with: url)
                .subscribe(onSuccess: { self.photoImageView.image = $0.image })
                .disposed(by: bag)
        }
    }
    
    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(photoImageView)
        photoImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
