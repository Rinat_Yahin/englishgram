//
//  SignUpSignUpController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class SignUpViewController: BaseViewController {

    // MARK: Properties
    private let viewModel: SignUpViewModel
    private let bag = DisposeBag()
    private lazy var rootView: SignUpView = {
        let view = SignUpView()
        view.backgroundColor = .white
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: SignUpViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
        bindViewModel()
    }
    
    override func loadView() {
        self.view = rootView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    private func bindView() {
        rootView.emailTextField.rx.text
            .orEmpty
            .bind(to: viewModel.email)
            .disposed(by: bag)

        rootView.usernameTextField.rx.text
            .orEmpty
            .bind(to: viewModel.username)
            .disposed(by: bag)

        rootView.passwordTextField.rx.text
            .orEmpty
            .bind(to: viewModel.password)
            .disposed(by: bag)
        
        rootView.plusPhotoButton.rx.tap.bind {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        }.disposed(by: bag)
        
        
        rootView.signUpButton.rx.tap.bind {
            self.setLoading(true)
            self.viewModel.signUp()
        }.disposed(by: bag)
        
        rootView.alreadyHaveAccountButton.rx.tap.bind {
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: bag)
    }

    private func bindViewModel() {
        viewModel
            .isEnabled
            .bind(to: rootView.signUpButton.rx.isEnabled)
            .disposed(by: bag)
        
        viewModel.errorMessage.subscribe(onNext: { error in
            self.presentMessage(message: error)
        }).disposed(by: bag)
    }
}

extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let editedImage = info[.editedImage] as? UIImage {
            rootView.plusPhotoButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        } else if let originalImage = info[.originalImage] as? UIImage {
            rootView.plusPhotoButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        rootView.plusPhotoButton.layer.cornerRadius = rootView.plusPhotoButton.frame.width/2
        rootView.plusPhotoButton.layer.masksToBounds = true
        rootView.plusPhotoButton.layer.borderColor = UIColor.black.cgColor
        rootView.plusPhotoButton.layer.borderWidth = 3
        guard let data = rootView.plusPhotoButton.imageView?.image else { return }
        viewModel.avatar = data.jpegData(compressionQuality: 0.3)
        dismiss(animated: true, completion: nil)
    }

}
