//
//  SignUpSignUpViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import Foundation
import RxSwift
import Firebase
import FirebaseDatabase
class SignUpViewModel {
    private let minimalPasswordLength = 5
    let email = BehaviorSubject<String>(value: "")
    let username = BehaviorSubject<String>(value: "")
    let password = BehaviorSubject<String>(value: "")
    let errorMessage = PublishSubject<String>()
    weak var coordinator: RootCoordinator?
    var avatar: Data?

    let isEnabled = BehaviorSubject<Bool>(value: false)
    private let bag = DisposeBag()
    private let userService: UserServiceProtocol
    private let storageService: StorageServiceProtocol
    private let dataBaseService: FirebaseDataBaseProtocol
    
    init(userService: UserServiceProtocol,
         storageService: StorageServiceProtocol,
         dataBaseService: FirebaseDataBaseProtocol) {
        self.userService = userService
        self.storageService = storageService
        self.dataBaseService = dataBaseService
        bindInput()
    }
    
    private func bindInput() {
        let emailValid = email
            .map { !$0.isEmpty }
            .share(replay: 1)

        let usernameValid = username
            .map { !$0.isEmpty }
            .share(replay: 1)
        
        let passwordValid = password
            .map { $0.count > self.minimalPasswordLength }
            .share(replay: 1)
        
        
        let everythingValid = Observable.combineLatest(emailValid, usernameValid, passwordValid) { $0 && $1 && $2 }
            .share(replay: 1)
        
        everythingValid.bind(to: isEnabled).disposed(by: bag)
    }
    
    func signUp() {
        guard let email = try? email.value(),
              let password = try? password.value(),
              let name = try? username.value()
        else {
            return
        }
        userService.createUser(email: email, password: password, name: name)
            .subscribe { result in
            switch result {
            case .success(let user):
                self.uploadAvatar(user: user)
                
            case .error(let error):
                self.errorMessage.onNext(error.localizedDescription)
            }
            }.disposed(by: bag)
    }
    
    func uploadAvatar(user: User) {
        guard let avatar = avatar else {
            self.errorMessage.onNext("Please choose avatar")
            return
        }
        storageService.putData(child: .profileImages,
                               fileName: UUID().uuidString,
                               uploadData: avatar).subscribe { result in
            switch result {
            case .success(let storageData):
                self.updateUser(user: user, storageData: storageData)
            case .error(let error):
                self.errorMessage.onNext(error.localizedDescription)
            }
            }.disposed(by: bag)
    }
    
    func updateUser(user: User, storageData: StorageData) {
        guard let uid = user.uid else { return }
        
        let dictionaryValues: [String: Any] = ["username": user.name, "profileImageUrl": storageData.url?.absoluteString]
        let values = [uid: dictionaryValues]
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.users.rawValue)
        dataBaseService.updateChildValues(ref: ref, values: values).subscribe { result in
            switch result {
            case .success:
                self.coordinator?.showMainScreen()
            case .error(let error):
                self.errorMessage.onNext(error.localizedDescription)
            }
        }.disposed(by: bag)
    }
}
