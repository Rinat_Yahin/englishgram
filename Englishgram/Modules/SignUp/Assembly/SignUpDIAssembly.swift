//
//  SignUpSignUpDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class SignUpDIAssembly: Assembly {
    func assemble(container: Container) {
        
        container.register(SignUpViewController.self) { (r: Resolver, coordinator: RootCoordinator) in
            let userService = r.resolve(UserServiceProtocol.self)!
            let storageService = r.resolve(StorageServiceProtocol.self)!
            let dataBaseService = r.resolve(FirebaseDataBaseProtocol.self)!
            let viewModel = SignUpViewModel(userService: userService,
                                            storageService: storageService,
                                            dataBaseService: dataBaseService)
            viewModel.coordinator = coordinator
            return SignUpViewController(viewModel: viewModel)
        }
    }
}
