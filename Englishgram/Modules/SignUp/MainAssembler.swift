//
//  MainAssembler.swift
//  Englishgram
//
//  Created by Rinat on 08.09.2021.
//

import Swinject

final class MainAssembler {
    
    static let shared = MainAssembler()
    let container = Container()
    
    var resolver: Resolver {
        return assembler.resolver
    }
    
    var assembler: Assembler
    
    private init() {
        assembler = Assembler([
            RootContainerDIAssembly(),
            SignInDIAssembly(),
            SignUpDIAssembly(),
            UserServiceAssembly(),
            StorageServiceAssembly(),
            FirebaseDataBaseAssembly(),
            SearchDIAssembly(),
            HomeDIAssembly(),
            UserProfileDIAssembly(),
            PhotoSelectorDIAssembly(),
            SharePhotoDIAssembly()
        ])
    }
}

