//
//  RootContainerRootContainerViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import Foundation
import Firebase

class RootContainerViewModel {
    var isAuthorized: Bool {
        return Auth.auth().currentUser != nil
    }
}
