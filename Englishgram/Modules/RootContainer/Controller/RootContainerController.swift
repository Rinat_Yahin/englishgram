//
//  RootContainerRootContainerController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit

class RootContainerViewController: BaseViewController {

    // MARK: Properties
    let viewModel: RootContainerViewModel

    private lazy var rootView: RootContainerView = {
        let view = RootContainerView()
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: RootContainerViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
