//
//  RootContainerRootContainerDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class RootContainerDIAssembly: Assembly {

    func assemble(container: Container) {
        container.register(RootContainerViewController.self, factory: { r in
            let viewModel = RootContainerViewModel()
            return RootContainerViewController(viewModel: viewModel)
        })
    }
    
}
