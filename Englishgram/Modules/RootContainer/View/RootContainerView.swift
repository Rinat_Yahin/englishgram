//
//  RootContainerRootContainerView.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit

class RootContainerView: BaseRootView {

    // MARK: Initialization
    init() {
        super.init(frame: .zero)
        setupViews()
    }

    private func setupViews() {
        self.backgroundColor = .white
    }

}
