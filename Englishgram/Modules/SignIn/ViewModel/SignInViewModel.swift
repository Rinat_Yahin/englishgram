//
//  SignInSignInViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import Foundation
import RxSwift
import Firebase
import FirebaseDatabase

class SignInViewModel {
    
    let email = BehaviorSubject<String>(value: "")
    let password = BehaviorSubject<String>(value: "")
    let errorMessage = PublishSubject<String>()
    let isEnabled = BehaviorSubject<Bool>(value: false)
    weak var coordinator: RootCoordinator?
    
    private let bag = DisposeBag()
    private let minimalPasswordLength = 5
    private let userService: UserServiceProtocol
    func goSignUp() {
        coordinator?.showSignUp()
    }
    
    init(userService: UserServiceProtocol) {
        self.userService = userService
        bindInput()
    }
    
    private func bindInput() {
        let emailValid = email
            .map { !$0.isEmpty }
            .share(replay: 1)

        let passwordValid = password
            .map { $0.count > self.minimalPasswordLength }
            .share(replay: 1)
        
        
        let everythingValid = Observable.combineLatest(emailValid,  passwordValid) { $0 && $1}
            .share(replay: 1)
        
        everythingValid.bind(to: isEnabled).disposed(by: bag)
    }
    
    func signIn() {
        guard let email = try? email.value(),
              let password = try? password.value()
        else {
            return
        }
        
        userService.login(email: email, password: password) { result in
            switch result {
            case .success:
                self.coordinator?.showMainScreen()
                
            case .failure(let error):
                self.errorMessage.onNext(error.localizedDescription)
            }
        }
    }

}
