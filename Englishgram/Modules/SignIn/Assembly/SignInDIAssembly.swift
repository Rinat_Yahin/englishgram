//
//  SignInSignInDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class SignInDIAssembly: Assembly {

    func assemble(container: Container) {
        
        container.register(SignInViewController.self) { (r: Resolver, coordinator: RootCoordinator) in
            let viewModel = SignInViewModel(userService: r.resolve(UserServiceProtocol.self)!)
            viewModel.coordinator = coordinator
            return SignInViewController(viewModel: viewModel)
        }

    }
    
}
