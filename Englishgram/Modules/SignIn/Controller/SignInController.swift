//
//  SignInSignInController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 08/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift
class SignInViewController: BaseViewController {

    // MARK: Properties
    private let viewModel: SignInViewModel
    private let bag = DisposeBag()

    private lazy var rootView: SignInView = {
        let view = SignInView()
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: SignInViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        bindView()
        bindViewModel()
    }
    
    override func loadView() {
        self.view = rootView
    }
    
    private func bindView() {
        rootView.emailTextField.rx.text
            .orEmpty
            .bind(to: viewModel.email)
            .disposed(by: bag)

        rootView.passwordTextField.rx.text
            .orEmpty
            .bind(to: viewModel.password)
            .disposed(by: bag)
        
        rootView.dontHaveAccountButton.rx.tap.bind {
            self.viewModel.goSignUp()
        }.disposed(by: bag)
        
        rootView.loginButton.rx.tap.bind {
            self.setLoading(true)
            self.viewModel.signIn()
        }.disposed(by: bag)
    }
    
    private func bindViewModel() {
        viewModel
            .isEnabled
            .bind(to: rootView.loginButton.rx.isEnabled)
            .disposed(by: bag)
        
        viewModel.errorMessage.subscribe(onNext: { error in
            self.presentMessage(message: error)
        }).disposed(by: bag)
    }

}
