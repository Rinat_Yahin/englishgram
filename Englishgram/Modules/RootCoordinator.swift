//
//  RootCoordinator.swift
//  Englishgram
//
//  Created by Rinat on 08.09.2021.
//

import Swinject

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    func start()
}

protocol RootCoordinatorProtocol: AnyObject {
    func didFinishPresentRootContainer(coordinator: Coordinator)
    func didFinishPresentLoginCoordinator(coordinator: Coordinator)
}

class RootCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    
    private var window: UIWindow?
    private let resolver = MainAssembler.shared.resolver
    private var loginNavigationViewController: UINavigationController!
    private var mainTabBarController: MainTabBarController!
    private var photoSelectorNavigationController: UINavigationController!
    private var searchViewController: UINavigationController!
    init(window: UIWindow?) {
        self.window = window
        self.window?.makeKeyAndVisible()
    }

    func start() {
        let viewController = resolver.resolve(RootContainerViewController.self)!
        window?.rootViewController = viewController
        
        if !viewController.viewModel.isAuthorized {
            showSigInScreen(rootController: viewController)
            return
        }
        showMainScreen()
    }

    func showSigInScreen(rootController: UIViewController) {
        let signInViewController = resolver.resolve(SignInViewController.self, argument: self)!
        loginNavigationViewController = UINavigationController(rootViewController: signInViewController)
        loginNavigationViewController.modalPresentationStyle = .fullScreen
        rootController.present(loginNavigationViewController, animated: true, completion: nil)
    }

    func showMainScreen() {
        mainTabBarController = MainTabBarController()
        mainTabBarController.loadViewIfNeeded()

        let homeViewController = UINavigationController(rootViewController: resolver.resolve(HomeViewController.self, argument: self)!)
        
        homeViewController.tabBarItem.image = #imageLiteral(resourceName: "home_unselected")
        homeViewController.tabBarItem.selectedImage = #imageLiteral(resourceName: "home_selected")
        
        searchViewController = UINavigationController(rootViewController: resolver.resolve(SearchViewController.self, argument: self)!)
        mainTabBarController.modalPresentationStyle = .fullScreen
        
        searchViewController.tabBarItem.image = #imageLiteral(resourceName: "search_unselected")
        searchViewController.tabBarItem.selectedImage = #imageLiteral(resourceName: "search_selected")
        
        let user: User? = nil
        let userController = resolver.resolve(UserProfileViewController.self, arguments: self, user)!
        let userNavigationController = UINavigationController(rootViewController: userController)
        
        userNavigationController.tabBarItem.image = #imageLiteral(resourceName: "profile_unselected")
        userNavigationController.tabBarItem.selectedImage = #imageLiteral(resourceName: "profile_selected")
        
        mainTabBarController.viewControllers = [homeViewController, searchViewController, userNavigationController]
        
        
        window?.rootViewController = mainTabBarController
    }
    
    func showSignUp() {
        let signUpViewController = resolver.resolve(SignUpViewController.self, argument: self)!
        loginNavigationViewController.pushViewController(signUpViewController, animated: true)
    }
    
    
    func showPhotoSelector() {
        let viewController = resolver.resolve(PhotoSelectorViewController.self, argument: self)!
        photoSelectorNavigationController = UINavigationController(rootViewController: viewController)
        photoSelectorNavigationController.modalPresentationStyle = .fullScreen
        mainTabBarController.present(photoSelectorNavigationController, animated: true, completion: nil)
    }
 
    func showSharePhoto(image: UIImage) {
        let viewController = resolver.resolve(SharePhotoViewController.self, arguments: self, image)!
        photoSelectorNavigationController.pushViewController(viewController, animated: true)
    }

    func dismissShareView() {
        photoSelectorNavigationController.dismiss(animated: true, completion: nil)
    }
    
    func openUserProfile(_ user: User?) {
        let viewController = resolver.resolve(UserProfileViewController.self, arguments: self, user)!
        searchViewController.pushViewController(viewController, animated: true)
    }
}
