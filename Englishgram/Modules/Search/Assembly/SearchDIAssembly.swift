//
//  SearchSearchDIAssembly.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import Swinject

class SearchDIAssembly: Assembly {

    func assemble(container: Container) {
        container.register(SearchViewController.self) { (r: Resolver, coordinator: RootCoordinator) in
            let viewModel = SearchViewModel(dataBaseService: r.resolve(FirebaseDataBaseProtocol.self)!)
            viewModel.coordinator = coordinator
            return SearchViewController(viewModel: viewModel)
        }
    }
}
