//
//  SearchSearchViewModel.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import Foundation
import Firebase
import RxSwift

class SearchViewModel {
    let usersSubject = PublishSubject<[User]>()
    let errorSubject = PublishSubject<String>()
    weak var coordinator: RootCoordinator?
    
    private let dataBaseService: FirebaseDataBaseProtocol
    private let bag = DisposeBag()

    init(dataBaseService: FirebaseDataBaseProtocol) {
        self.dataBaseService = dataBaseService
    }

    func fetchUsers() -> Single<[User]> {
        let ref = Database.database().reference().child(FirebaseDataBaseChildType.users.rawValue)
        return dataBaseService
            .observeSingleEvent(ref: ref).map { snapshot in
                guard let dictionaries = snapshot.value as? [String: Any] else { return [] }
                var users = [User]()
                dictionaries.forEach({ (key, value) in
                    guard let userDictionary = value as? [String: Any] else { return }
                    if key == Auth.auth().currentUser?.uid {
                        return
                    }
                    
                    let user = User(uid: key, dictionary: userDictionary)
                    users.append(user)
                })
                return users
        }
    }
    
    func goUserProfile(_ user: User) {
        coordinator?.openUserProfile(user)
    }
}
