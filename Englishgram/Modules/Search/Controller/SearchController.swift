//
//  SearchSearchController.swift
//  Englishgram
//
//  Created by Rinat Iakhin on 10/09/2021.
//  Copyright © 2021 Rinat inc. All rights reserved.
//

import UIKit
import RxSwift
class SearchViewController: BaseViewController {

    // MARK: Properties
    private let viewModel: SearchViewModel
    private let bag = DisposeBag()
    private lazy var rootView: SearchView = {
        let view = SearchView()
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        rootView.addSearchBar(navigationController?.navigationBar)
        bindViewModel()
        bindView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        rootView.searchBar.isHidden = false
    }
    
    override func loadView() {
        self.view = rootView
    }
    
    func bindView() {
        rootView.userSubject.subscribe { user in
            self.viewModel.goUserProfile(user)
        }.disposed(by: bag)
    }
    
    func bindViewModel() {
        viewModel.fetchUsers().subscribe {
            switch $0 {
            case .success(let users):
                self.rootView.users = users
             
            case.error(let error):
                self.present(error: error)
            }
        }.disposed(by: bag)
    }
}
