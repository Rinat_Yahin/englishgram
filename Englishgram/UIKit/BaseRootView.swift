//
//  UIView.swift
//  wrf
//
//  Created by Rinat on 13/12/2018.
//  Copyright © 2018 Gstudio. All rights reserved.
//

import UIKit

open class BaseRootView: UIView {
    
    // MARK: - Methods
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @available(*, unavailable,
    message: "Loading this view from a nib is unsupported in favor of initializer dependency injection."
    )
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Loading this view from a nib is unsupported in favor of initializer dependency injection.")
    }
}
