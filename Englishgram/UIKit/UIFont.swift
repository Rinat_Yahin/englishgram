//
//  UIFont.swift
//  ECExtra
//
//  Created by Rinat on 11.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

extension UIFont {
    
    enum FontName {
        
        case bold
        case regular
        var type: String {
            switch self {
                
            case .bold:
                return "PTSans-Bold"
            case .regular:
                return "PTSans-Regular"
            }
        }
    }
    
    static func font(name: FontName, size: CGFloat) -> UIFont {
        return UIFont(name: name.type, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func showAllFonts() {
          for family: String in UIFont.familyNames
          {
              print(family)
              for names: String in UIFont.fontNames(forFamilyName: family)
              {
                  print("== \(names)")
              }
          }
      }
}

