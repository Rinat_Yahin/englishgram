//
//  UIAlertController.swift
//  ECExtra
//
//  Created by Rinat on 21.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func message(message: String, okTitle: String?, cancelTitle: String?,
                        okHandler: ((UIAlertAction) -> Void )? = nil, cancelHandler: ((UIAlertAction) -> Void )? = nil) -> UIAlertController {
        
        let alert = UIAlertController(title: nil,
                                      message: message,
                                      preferredStyle: .alert)
        if let okTitle = okTitle {
            let okAlert = UIAlertAction(title: okTitle, style: .default,
                                        handler: okHandler)
            alert.addAction(okAlert)
        }
        if let cancel = cancelTitle {
            let cancelAlert = UIAlertAction(title: cancel,
                                            style: .cancel,
                                            handler: cancelHandler)
            alert.addAction(cancelAlert)
        }
        return alert
    }
}

extension UIAlertController {
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.tintColor = .hex_253B75
        
    }
}

