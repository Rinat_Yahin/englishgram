//
//  UITableViewCell.swift
//  ECExtra
//
//  Created by Rinat on 22.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var cellReuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: cellReuseIdentifier, bundle: nil)
    }
}

extension UITableViewHeaderFooterView {
    
    static var viewReuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: viewReuseIdentifier, bundle: nil)
    }
}

