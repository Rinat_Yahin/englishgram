//
//  BaseViewController.swift
//  wrf
//
//  Created by Rinat on 13/12/2018.
//  Copyright © 2018 Gstudio. All rights reserved.
//

import UIKit

open class BaseViewController: UIViewController {
    
    // MARK: - Methods
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable,
    message: "Loading this view controller from a nib is unsupported in favor of initializer dependency injection."
    )
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    @available(*, unavailable,
    message: "Loading this view controller from a nib is unsupported in favor of initializer dependency injection."
    )
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Loading this view controller from a nib is unsupported in favor of initializer dependency injection.")
    }

    
    
    
//    override open func transition(from fromViewController: UIViewController, to toViewController: UIViewController, duration: TimeInterval, options: UIView.AnimationOptions = [], animations: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
//        print("|from: fromViewController, to: toViewController, duration: duration, options: options, animations: animations, completion: completion")
//        super.transition(from: fromViewController, to: toViewController, duration: duration, options: options, animations: animations, completion: completion)
//    }
//    
//    open override func willMove(toParent parent: UIViewController?) {
//        print(parent)
//        super.willMove(toParent: parent)
//    }
//    
//    override open func didMove(toParent parent: UIViewController?) {
//        print(parent)
//        super.didMove(toParent: parent)
//    }
    

}
