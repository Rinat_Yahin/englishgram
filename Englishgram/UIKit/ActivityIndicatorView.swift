//
//  ActivityIndicatorView.swift
//  ECExtra
//
//  Created by Rinat on 21.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

final class ActivityIndicatorView: UIView {
    
    private(set) lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    lazy var backgroundView: UIView = {
        let view = UIView()
        return view
    }()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        addSubview(backgroundView)
        addSubview(indicator)
        
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            backgroundView.leftAnchor.constraint(equalTo: leftAnchor),
            backgroundView.topAnchor.constraint(equalTo: topAnchor),
            backgroundView.rightAnchor.constraint(equalTo: rightAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])

        indicator.color = .hex_253B75
        backgroundView.backgroundColor = .white
        backgroundView.alpha = 0.6
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        guard (superview != nil) else { return }
       
        frame = CGRect(x: 0, y: 0, width: 300, height: 300)
//        NSLayoutConstraint.activate([
//
//            leftAnchor.constraint(equalTo: leftAnchor),
//            topAnchor.constraint(equalTo: topAnchor),
//            rightAnchor.constraint(equalTo: rightAnchor),
//            bottomAnchor.constraint(equalTo: bottomAnchor),
//        ])
    }
    
    func showLoader() {
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(self)
            window.bringSubviewToFront(self)
        }
        isHidden = false
        backgroundView.isHidden = false
        indicator.startAnimating()
        isUserInteractionEnabled = true
    }
    
    func hideLoader() {
        removeFromSuperview()
        isHidden = true
        backgroundView.isHidden = true
        indicator.stopAnimating()
        isUserInteractionEnabled = false
    }
    
}
