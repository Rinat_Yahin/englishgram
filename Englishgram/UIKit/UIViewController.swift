//
//  UIViewController.swift
//  ECExtra
//
//  Created by Rinat on 21.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

enum AlertLoaderType {
    case show
    case hide
    case showMessage(meessage: String)
}

typealias AlertLoaderPresentable = AlertPresentable & LoaderPresentable

protocol LoaderPresentable: AnyObject {
    func setLoading(_ loading: Bool)
}

protocol AlertPresentable: AnyObject {
    func present(error: Error)
}


extension UIViewController: LoaderPresentable {
    
    private enum AssociatedKeys {
        static var indicatorKey: Int = 0
    }
    
    private var indicator: ActivityIndicatorView {
        get {
            guard let value = objc_getAssociatedObject(self,
                                                       &AssociatedKeys.indicatorKey) as? ActivityIndicatorView
                else {
                    let indicator = ActivityIndicatorView()
                    indicator.translatesAutoresizingMaskIntoConstraints = false
                    self.indicator = indicator
                    return indicator
            }
            return value
        }
        set {
            objc_setAssociatedObject(self,
                                     &AssociatedKeys.indicatorKey,
                                     newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func setLoading(_ loading: Bool) {
        DispatchQueue.main.async { [weak self] in
            if loading {
                self?.indicator.showLoader()
            } else {
                self?.indicator.hideLoader()
            }
        }
    }
    
    
    func setLoading(_ type: AlertLoaderType) {
        DispatchQueue.main.async { [weak self] in
            switch type {
            case .show:
                self?.indicator.showLoader()
            case .hide:
                self?.indicator.hideLoader()
            case .showMessage(let error):
                self?.indicator.hideLoader()
                self?.presentMessage(message: error)
            }
        }
    }
    
    
    func presentMessage(message: String) {
        DispatchQueue.main.async { [weak self] in
            self?.setLoading(false)
            let alert = UIAlertController.message(message: message, okTitle: "Ok", cancelTitle: nil)
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    
}

extension UIViewController: AlertPresentable {
    
    func present(error: Error) {
        DispatchQueue.main.async { [weak self] in
            self?.setLoading(false)
            let alert = UIAlertController.message(message: error.localizedDescription, okTitle: "Ok", cancelTitle: nil)
            self?.present(alert, animated: true, completion: nil)
        }
    }
}



extension UIViewController {
    
    func addLeftButton(image: UIImage) {
        let space = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        space.width = -5
        let item = UIBarButtonItem(image: image.withRenderingMode(.alwaysOriginal),
                                   style: .plain, target: self, action: #selector(close))
        
        navigationItem.leftBarButtonItems = [space, item]
    }
    
    func addBackButton(image: UIImage) {
           let space = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
           space.width = -5
           let item = UIBarButtonItem(image: image.withRenderingMode(.alwaysOriginal),
                                      style: .plain, target: self, action: #selector(back))
           
           navigationItem.leftBarButtonItems = [space, item]
       }
    
    @objc private func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func back() {
        self.navigationController?.popViewController(animated: true)
    }
}
