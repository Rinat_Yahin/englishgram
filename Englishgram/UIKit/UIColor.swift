//
//  UIColor.swift
//  ECExtra
//
//  Created by Rinat on 11.03.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var hex_253B75: UIColor {
        return #colorLiteral(red: 0.1450980392, green: 0.231372549, blue: 0.4588235294, alpha: 1)
    }
    
    static var hex_579F2B: UIColor {
        return #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
    }
    
    static var hex_FED11A: UIColor {
        return #colorLiteral(red: 0.9960784314, green: 0.8196078431, blue: 0.1019607843, alpha: 1)
    }
    
    static var hex_253B86: UIColor {
        return #colorLiteral(red: 0.1450980392, green: 0.231372549, blue: 0.5254901961, alpha: 0.4803884846)
    }
    
    static var hex_3b5998: UIColor {
        return #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
    }
    
    
}
